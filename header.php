<!doctype html>
<html class="no-js">
  <head>
    <meta charset="utf-8">
    <title><?php wp_title('&laquo;', true, 'right'); ?> <?php bloginfo('name'); ?></title>

    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width,initial-scale=1">

    <link href="//www.google-analytics.com" rel="dns-prefetch">
    <link href="//ajax.googleapis.com" rel="dns-prefetch">
    <link rel="stylesheet" href="<?php bloginfo('stylesheet_url'); ?>"/>
    <link rel="pingback" href="<?php bloginfo('pingback_url'); ?>" />

    <!-- Google Fonts: Oswald - light, normal & bold -->
    <link href='http://fonts.googleapis.com/css?family=Oswald:400,300,700' rel='stylesheet' type='text/css'>

    <script src="<?php bloginfo('template_url');?>/assets/components/modernizr/modernizr-2.8.3.min.js"></script>

    <?php if ( is_singular() ) wp_enqueue_script( 'comment-reply' ); ?>
    <?php wp_head(); ?>
  </head>

  <body <?php body_class(); ?>>

  <header class="header" role="banner">
    <div class="container grid">
      <a class="logo grid-4-of-12" href="<?php bloginfo('url');?>">
        <img src="<?php bloginfo('template_url');?>/assets/img/logo.jpg" alt="<?php bloginfo('title');?>"/>
      </a><!--

   --><nav class="nav nav--main grid-8-of-12" role="navigation">
        <?php wp_nav_menu( array('menu' => 'Main', 'container' => false, )); ?>
      </nav>
    </div>
  </header>
