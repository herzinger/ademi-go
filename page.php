<?php get_header(); ?>

  <div class="container grid">

    <div class="grid-3-of-12">
      <?php get_sidebar(); ?>
    </div><!--

 --><div class="grid-9-of-12">
      <main class="main">
        <?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>
          <h1><?php the_title();?></h1>
          <?php the_content();?>
        <?php endwhile; ?>
      </main>
    </div>

  </div>

<?php get_footer(); ?>
