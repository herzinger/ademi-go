    <footer class="footer" role="contentinfo"></footer>

    <!-- Google Analytics: change UA-XXXXX-X to be your site's ID. -->
    <script>
        window._gaq = [['_setAccount','UAXXXXXXXX1'],['_trackPageview'],['_trackPageLoadTime']];
        Modernizr.load({
            load: ('https:' == location.protocol ? '//ssl' : '//www') + '.google-analytics.com/ga.js'
        });
    </script>

    <?php wp_footer(); ?>

  </body>
</html>
